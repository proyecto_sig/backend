CREATE TABLE IF NOT EXISTS usuario (
    idUsuario  SERIAL  NOT NULL PRIMARY KEY,
    username  VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS grupo(
    idGrupo  SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    tag VARCHAR(25)
);

CREATE TABLE IF NOT EXISTS codigo(
    idCodigo SERIAL NOT NULL PRIMARY KEY,
    codigo VARCHAR(50) NOT NULL,
    rol VARCHAR(50) NOT NULL,
    idGrupo INTEGER NOT NULL REFERENCES grupo(idGrupo)
);

CREATE TABLE IF NOT EXISTS grupoUsuario(
    idGU SERIAL NOT NULL PRIMARY KEY,
    idUsuario INTEGER REFERENCES usuario(idUsuario),
    idGrupo INTEGER REFERENCES grupo(idGrupo)
);

ALTER TABLE grupo
ADD COLUMN funcionalidad INTEGER NOT NULL


ALTER TABLE grupoUsuario
DROP CONSTRAINT grupousuario_idgrupo_fkey;

ALTER TABLE grupoUsuario
ADD CONSTRAINT grupousuario_idgrupo_fkey
FOREIGN KEY (idGrupo)
REFERENCES grupo(idGrupo)
ON DELETE CASCADE;


ALTER TABLE grupoUsuario
DROP CONSTRAINT grupousuario_idusuario_fkey;

ALTER TABLE grupoUsuario
ADD CONSTRAINT grupousuario_idusuario_fkey
FOREIGN KEY (idUsuario)
REFERENCES usuario(idUsuario)
ON DELETE CASCADE;



ALTER TABLE codigo
DROP CONSTRAINT codigo_idgrupo_fkey;

ALTER TABLE codigo
ADD CONSTRAINT codigo_idgrupo_fkey
FOREIGN KEY (idUsuario)
REFERENCES usuario(idUsuario)
ON DELETE CASCADE;




CREATE TABLE localizacion(
	idLocal SERIAL NOT NULL PRIMARY KEY,
	idUsuario INTEGER REFERENCES usuario(idUsuario),
	latitud FLOAT,
	longitud FLOAT,
	tiempo TIMESTAMP WITH TIME ZONE
);

CREATE TABLE eventos(
	id INT PRIMARY KEY NOT NULL,
	id_usuario INT REFERENCES usuario(idusuario) NOT NULL,
	id_catastro INT REFERENCES catastro_municipal(id) NOT NULL,
	tiempo TIMESTAMP WITH TIME ZONE NOT NULL
);

SELECT now()



















