CREATE OR REPLACE FUNCTION getBoundingBox()
RETURNS TABLE(xmin double precision	, ymax double precision	, ancho double precision, alto double precision	)
AS $$
BEGIN
	RETURN QUERY
	SELECT  ST_Xmin(bb) AS xmin, 
        	ST_ymax(bb)*-1 AS ymax, 
        	ST_Xmax(bb)-ST_Xmin(bb) AS ancho,
        	ST_Ymax(bb)-ST_ymin(bb) AS alto
	FROM 
      (SELECT ST_Extent(geom) AS bb 
	   FROM catastro_municipal) AS extent;

END;
$$
LANGUAGE plpgsql;

SELECT * FROM getBoundingBox();

/*****************************************************/
CREATE OR REPLACE FUNCTION insertarUsuario
(userN VARCHAR(100),
v_latitud FLOAT,
v_longitud FLOAT)
RETURNS boolean 
AS $$
DECLARE idU INTEGER;
BEGIN
	IF NOT EXISTS (SELECT idUsuario FROM usuario WHERE username = userN)
	THEN
		BEGIN
			INSERT INTO usuario(username) VALUES (userN);
			idU:= (SELECT idUsuario FROM usuario ORDER BY idUsuario DESC LIMIT 1);
			CALL updateLocation(idU,v_latitud,v_longitud);
			RETURN true;
		END;
	ELSE
		RETURN false;
	END IF;
END;
$$
LANGUAGE plpgsql;

SELECT insertarusuario AS success FROM  insertarUsuario('Edagrdo',10.0, 20.0);
/*************************************************************************************************/
CREATE OR REPLACE FUNCTION insertarGrupo
(v_nombreGrupo VARCHAR(100),
 v_tag VARCHAR(25),
 v_codigo VARCHAR(50),
 v_funcionalidad INTEGER)
RETURNS boolean
AS $$
DECLARE idG INTEGER;
BEGIN
	IF NOT EXISTS (SELECT idGrupo FROM grupo WHERE tag = v_tag )
	THEN
		BEGIN
			INSERT INTO grupo(nombre, tag, funcionalidad)
			VALUES(v_nombreGrupo, v_tag, v_funcionalidad);
			
			idG:= (SELECT idGrupo FROM grupo ORDER BY idGrupo DESC LIMIT 1);
			
			INSERT INTO codigo (codigo, rol, idGrupo)
			VALUES (v_codigo, 1, idG);
			RETURN true;
		END;
	ELSE
		RETURN false;
	END IF;
END;
$$
LANGUAGE plpgsql;


SELECT insertarGrupo AS success FROM insertarGrupo('Grupo 2', 'G2', 'G02',1);

/**********************************************************************************/

CREATE OR REPLACE FUNCTION insertarCodigo
(v_idGrupo INTEGER,
 v_codigo VARCHAR(50),
 v_rol VARCHAR(50))
RETURNS boolean
AS $$
BEGIN
	IF NOT EXISTS(SELECT idCodigo FROM codigo WHERE codigo = v_codigo)
	THEN
		BEGIN
			INSERT INTO codigo (codigo, rol, idGrupo)
			VALUES (v_codigo, v_rol, v_idGrupo);
			RETURN true;
		END;
	ELSE
		RETURN false;
	END IF;
END;
$$
LANGUAGE plpgsql;

select * from grupo

SELECT insertarcodigo AS success FROM insertarCodigo(8,'G2 HOUSE','1' )


/******************************************************************************/

CREATE OR REPLACE FUNCTION asociarUsuario
(v_usuario VARCHAR(100),
 v_tag VARCHAR(50))
RETURNS boolean
AS $$
DECLARE idU INTEGER; --Id del usuario
DECLARE idG INTEGER; --Id del grupo
BEGIN
	
	idU:= (SELECT idUsuario FROM usuario WHERE username = v_usuario);
	idG:= (SELECT idGrupo FROM grupo WHERE tag = v_tag);
	
	IF (idU IS NULL OR idG IS NULL)
	THEN
		RETURN false;
	ELSE
		IF EXISTS (SELECT idgu FROM grupoUsuario WHERE idUsuario = idU AND idGrupo = idG)
		THEN
			RETURN false;
		ELSE
			BEGIN
				INSERT INTO grupoUsuario(idUsuario,idGrupo)
				VALUES(idU,idG);
				RETURN true;
			END;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql;
SELECT * FROM grupoUsuario
SELECT asociarUsuario AS succes FROM asociarUsuario('Daniel','G2');
/******************************************************************/

CREATE OR REPLACE FUNCTION get_codigoGrupo
(v_idGrupo INTEGER)
RETURNS TABLE(g_idCodigo INTEGER, g_codigo VARCHAR(50), g_rol VARCHAR(50))
AS $$
BEGIN
	RETURN QUERY
	SELECT idCodigo, codigo, rol
	FROM codigo
	WHERE idGrupo = v_idGrupo;
END;
$$
LANGUAGE plpgsql;

SELECT * FROM get_codigoGrupo(6);

/******************************************************************/
CREATE OR REPLACE FUNCTION get_Grupo
(v_tag VARCHAR(25),
 v_codigo VARCHAR(50))
RETURNS TABLE(g_idGrupo INTEGER, g_nombre VARCHAR(50), g_funcionalidad INTEGER)
AS $$
BEGIN
	RETURN QUERY
	SELECT g.idGrupo, g.nombre, g.funcionalidad
	FROM grupo AS g
	INNER JOIN codigo AS c
	ON g.idGrupo = c.idGrupo
	WHERE c.codigo = v_codigo
	AND g.tag = v_tag;

END;
$$
LANGUAGE plpgsql;

SELECT * FROM get_Grupo('g1','nose');
/******************************************************************/

CREATE OR REPLACE FUNCTION delete_Codigo
(v_idCodigo INTEGER)
RETURNS boolean
AS $$
BEGIN
	IF EXISTS (SELECT codigo FROM codigo WHERE idCodigo = v_idCodigo)
	THEN
		BEGIN
			DELETE FROM codigo
			WHERE idCodigo = v_idCodigo;
			RETURN true;
		END;
	ELSE
		RETURN false;
	END IF;
END;
$$
LANGUAGE plpgsql;

SELECT * FROM delete_Codigo(1)


/******************************************************************/

CREATE OR REPLACE PROCEDURE updateLocation
(v_idUsuario INTEGER,
v_latitud FLOAT,
v_longitud FLOAT)
AS $$
BEGIN
	IF EXISTS(SELECT idUsuario FROM localizacion WHERE idUsuario = v_idUsuario)
	THEN
		UPDATE localizacion
		SET latitud = v_latitud,
			longitud = v_longitud,
			tiempo = now()
		WHERE idUsuario = v_idUsuario;
	ELSE
		INSERT INTO localizacion(idUsuario,latitud, longitud, tiempo)
		VALUES(v_idUsuario,v_latitud, v_longitud, now());
	END IF;
END;
$$
LANGUAGE plpgsql;

/******************************************************************/

CREATE OR REPLACE FUNCTION verificarCodigo
(v_tag VARCHAR(25),
 v_codigo VARCHAR(50),
 v_username VARCHAR(100))
 RETURNS TABLE (g_idGrupo INTEGER, g_nombre VARCHAR(50), g_funcionalidad INTEGER, c_rol VARCHAR(50))
 AS $$
 DECLARE v_idGrupo INTEGER;
 DECLARE v_idCodigo INTEGER;
 DECLARE v_idUser INTEGER;
 BEGIN
 	v_idGrupo := (SELECT idGrupo FROM grupo WHERE tag = v_tag);
	v_idUser := (SELECT idUsuario FROM usuario WHERE username = v_username);
	
	IF (v_idGrupo IS NOT NULL)
	THEN
		
		v_idCodigo := (SELECT idCodigo FROM codigo WHERE idGrupo = v_idGrupo AND codigo = v_codigo);
		IF (v_idCodigo IS NOT NULL)
		THEN
		
			IF NOT EXISTS (SELECT * FROM grupoUsuario WHERE idGrupo = v_idGrupo AND idUsuario = v_idUser)
			THEN
				IF EXISTS (	SELECT g.idGrupo, g.nombre, g.funcionalidad, c.rol
						FROM grupo AS g
						INNER JOIN codigo AS c
						ON c.idGrupo = g.idGrupo
						WHERE g.idGrupo = v_idGrupo
						AND c.idCodigo = v_idCodigo)
				THEN
					INSERT INTO grupoUsuario(idGrupo, idUsuario)
					VALUES(v_idGrupo,v_idUser);
				END IF;
			END IF;
			
			RETURN QUERY
			SELECT g.idGrupo, g.nombre, g.funcionalidad, c.rol
			FROM grupo AS g
			INNER JOIN codigo AS c
			ON c.idGrupo = g.idGrupo
			WHERE g.idGrupo = v_idGrupo
			AND c.idCodigo = v_idCodigo;
		END IF;
		
	END IF;
 
 END;
 $$
 LANGUAGE plpgsql;



SELECT * FROM verificarCodigo('G2','G2 HOUSE','Fran');



CREATE OR REPLACE FUNCTION eventoOnRango(inicio TIMESTAMP WITH TIME ZONE,fin TIMESTAMP WITH TIME ZONE)
RETURNS table(id int, id_usuario int , id_catastro int,tiempo  timestamp with time zone) AS $$
BEGIN
RETURN QUERY
  SELECT * FROM eventos as e WHERE e.tiempo < fin AND e.tiempo > inicio;
END;
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION localOnRango(inicio TIMESTAMP WITH TIME ZONE,fin TIMESTAMP WITH TIME ZONE)
RETURNS table( idlocal int, idusuario int, latitud double precision, longitud double precision,tiempo  timestamp with time zone) AS $$
BEGIN
RETURN QUERY
  SELECT * FROM localizacion as l WHERE l.tiempo < fin AND l.tiempo > inicio;
END;
$$
LANGUAGE 'plpgsql';

SELECT * FROM eventoOnRango('2020-10-11 08:10:20-06', '2020-10-11 08:30:20-06')







