var express = require('express');
var router = express.Router();
var pruebaManager = require('../managers/pruebaManager');

router.get('/getBoundingBox', (req,res,next) =>{
    try {
        pruebaManager.getBoundingBox()
        .then((result) => {

            res.status(200).json({ result });
        })
        .catch((err) => {
            return console.log(err  );
    })

    } catch (error) {
        console.log(error);
    }
    
});

router.get('/getFigureSVG', (req,res,next) =>{
    try {
        pruebaManager.getFigureSVG()
        .then((result) => {
            res.status(200).json({ result });
        })
        .catch((err) => {
            return console.log(err  );
    })

    } catch (error) {
        console.log(error);
    }
    
});

router.post('/insertarUsuario', (req,res,next) =>{
    console.log(req.body)
    pruebaManager.insertarUsuario(req.body)
    .then((result) => {
        res.status(200).json({ Success:result.success });
    })
    .catch((err) => {
        return console.log(err );
    })
});

router.post('/insertarGrupo', (req,res,next) =>{
    console.log(req.body)
    pruebaManager.insertarGrupo(req.body)
    .then((result) => {
        res.status(200).json({ Success:result.success });
    })
    .catch((err) => {
        return console.log(err );
    })
});

router.post('/insertarCodigo', (req,res,next) =>{
    console.log(req.body)
    pruebaManager.insertarCodigo(req.body)
    .then((result) => {
        res.status(200).json({ Success:result.success });
    })
    .catch((err) => {
        return console.log(err );
    })
});
router.post('/asociarUsuario', (req,res,next) =>{
    console.log(req.body)
    pruebaManager.asociarUsuario(req.body)
    .then((result) => {
        res.status(200).json({ Success:result.success });
    })
    .catch((err) => {
        return console.log(err );
    })
});

router.get('/getCodigoGrupo', (req,res,next) =>{
    pruebaManager.getCodigoGrupo(req.query)
    .then((result) => {
        res.status(200).json({ result });
    })
    .catch((err) => {
        return console.log(err );
    })
});

router.get('/getGrupo', (req,res,next) =>{
    pruebaManager.getGrupo(req.query)
    .then((result) => {
        res.status(200).json({ result });
    })
    .catch((err) => {
        return console.log(err );
    })
});

router.post('/deleteCodigo', (req,res,next) =>{
    console.log(req.body)
    pruebaManager.deleteCodigo(req.body)
    .then((result) => {
        res.status(200).json({ Success:result.success });
    })
    .catch((err) => {
        return console.log(err );
    })
});

router.post('/verificarCodigo', (req,res,next) =>{
    console.log(req.body)
    pruebaManager.verificarCodigo(req.body)
    .then((result) => {
        if(result == null)
            res.status(200).json({ Success:false });
        else
            res.status(200).json({ Grupo: result });
    })
    .catch((err) => {
        return console.log(err );
    })
});
module.exports = router;