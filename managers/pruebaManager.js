const { Pool } = require('pg');
var connection = require('../config/connection');
var pool  = connection.pool;
var poolProfe = connection.poolProfe


exports.getBoundingBox = async () =>{
    const res = await pool.query('SELECT * FROM getBoundingBox()');
    return res.rows;
}


exports.getFigureSVG = async () =>{
    const res = await connection.poolProfe.query("SELECT ST_ASSVG(geom), finca, direccion, declara, plano_cata FROM idesca.catastro_municipal LIMIT 2000");
    return res.rows;
}

//Buscar mejores returns....
exports.insertarUsuario = async(req) =>{
    const texto = "SELECT insertarusuario AS success FROM  insertarUsuario('" + req.nombre + "','" + req.latitud + "','" + req.longitud +"');";
    console.log(texto);
    const res = await pool.query(texto);
    console.log(res.rows[0])
    return res.rows[0]
}

exports.insertarGrupo = async(req) =>{
    const texto = "SELECT insertarGrupo AS success FROM insertarGrupo('" + req.nombreGrupo + "','" + req.tag + "','" + req.codigo + "'," + req.funcionalidad +");";
    console.log(texto);
    const res = await pool.query(texto);
    return res.rows[0]
}
exports.insertarCodigo = async(req) =>{
    const texto = "SELECT insertarcodigo AS success FROM insertarCodigo("+req.idGrupo +",'"  + req.nombre + "','" + req.rol + "');";
    console.log(texto);
    const res = await pool.query(texto);
    return res.rows[0]
}

exports.asociarUsuario = async(req) =>{
    const texto = "SELECT asociarUsuario AS success FROM asociarUsuario('" + req.nombre + "','" + req.grupo + "');";
    console.log(texto);
    const res = await pool.query(texto);
    return res.rows[0]
}


exports.getCodigoGrupo = async(req) =>{
    const texto = "SELECT * FROM get_codigoGrupo('" + req.idGrupo + "');";
    const res = await pool.query(texto);
    return res.rows
}

exports.getGrupo = async(req) =>{
    const texto = "SELECT * FROM get_Grupo('" + req.tag +  "','" + req.codigo + "');";
    const res = await pool.query(texto);
    return res.rows[0]
}

exports.deleteCodigo = async(req) =>{
    const texto = "SELECT delete_codigo AS success FROM delete_Codigo('" + req.idCodigo + "');";
    const res = await pool.query(texto);
    return res.rows[0]
}

exports.verificarCodigo = async(req) =>{
    const texto = "SELECT * FROM verificarCodigo('" + req.tag + "','" + req.codigo + "','" + req.usuario +"');";
    const res = await pool.query(texto);
    return res.rows[0]
}