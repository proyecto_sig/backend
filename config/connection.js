var pg = require('pg');

const connectionData = {
    user: 'postgres',
    host: 'localhost',
    database: 'proyecto_sig',
    password: '12345',
    port: 5432,
};

const connectionProfe = {
    user: 'gis',
    host: 'leoviquez.com',
    database: 'gis',
    password: '12345',
    port: 5432,
};



var poolProfe = new pg.Pool(connectionProfe);

var pool = new pg.Pool(connectionData);



module.exports = {pool, poolProfe};